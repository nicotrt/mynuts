# MyNuts

Site réalisé  en autonomie dans le cadre de mon apprentissage du framework Symfony.

Le but est de créer un site permettant à chacun de mes collègues, lors de ses recherches de stage, d'enregistrer des entreprises pouvant intéresser d'autres personnes.

WIP : le site en est actuellement à sa V1.0 => n'hésitez pas à proposer des améliorations et fonctionnalités, je ferai de mon mieux pour trouver le temps de l'améliorer.