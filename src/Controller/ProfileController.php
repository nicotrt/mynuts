<?php

namespace App\Controller;

use App\Entity\Applications;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index()
    {
        $userID = $this->getUser()->getID();
        $applications = $this->getDoctrine()->getRepository(Applications::class)->findBy(['userID' => $userID]);
        $applicationsList = array();
//        dump($applications[0]->getListID()->getCompanyName()); die;
        for ($i= 0; $i < count($applications); $i++) {
            array_push($applicationsList, $applications[$i]);
        }

        return $this->render('profile/profile.html.twig', [
            'applications' => $applicationsList,
        ]);
    }
}
