<?php

namespace App\Controller;

use App\Entity\Applications;
use App\Entity\Listing;
use App\Entity\User;
use App\Form\ListingType;
use App\Repository\ListingRepository;
use App\Repository\UserRepository;
use App\Services\ApplicationChecking;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     * @param ListingRepository $listingRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return Response
     */
    public function index(ListingRepository $listingRepository, Request $request, ObjectManager $manager)
    {
        $listing = $listingRepository->findAll();

        $list = new Listing();

        $listingForm = $this->createForm(ListingType::class, $list);

        $listingForm->handleRequest($request);

        if ($listingForm->isSubmitted() && $listingForm->isValid()) {

            $manager->persist($list);
            $manager->flush();

            $this->addFlash(
                'success',
                'Entreprise enrergistrée, merci de ta participation !'
            );

            return $this->redirectToRoute('list');
        }

        $this->flag = false;

        return $this->render('list/list.html.twig', [
            "listing" => $listing,
            "listingForm" => $listingForm->createView(),
        ]);
    }

    /**
     * @Route("/list/apply/{listID}", name="apply")
     * @param $listID
     * @param ApplicationChecking $applicationChecking
     * @return RedirectResponse
     */
    public function addCandidate($listID, ApplicationChecking $applicationChecking) {

        $userID = $this->getUser()->getID();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userID);
        $list = $this->getDoctrine()->getRepository( Listing::class)->find($listID);

        if ($applicationChecking->checkApplication($userID, $listID)) {
            $this->addFlash(
                'warning',
                'Tu as déjà envoyé une candidature pour cette entreprise !'
            );
        } else {
            $application = new Applications();
            $application->setListID($list);
            $application->setUserID($user);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($application);
            $manager->flush();

            $this->addFlash(
                'success',
                'Ta candidature a bien été prise en compte !'
            );
        }
        return $this->redirectToRoute('list');
    }
}
