<?php

namespace App\Services;

use App\Entity\Applications;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApplicationChecking extends AbstractController
{
    private $flag;
    public function checkApplication($userID, $listID)
    {
        settype($listID, "integer");
//        dump($userID);
//        dump($listID);
        $applying = $this->getDoctrine()->getRepository(Applications::class)->findAll();
        foreach ($applying as $key) {
//            dump($key->getListID()->getId());
            if ($key->getUserID()->getId() == $userID && $key->getListID()->getId() == $listID) {
                $this->flag = true;
                break;
            } else {
                $this->flag = false;
            }
        }
        return $this->flag;
    }
}